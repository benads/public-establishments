<?php
namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GeoApi extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function home(Request $request)
    {
       $types = ["caf" => "caf", "mairie" => "mairie","prefecture"=>"prefecture"];

        $form = $this->createFormBuilder()
            ->add('township', TextType::class)
            ->add('code', TextType::class)
            ->add('type', ChoiceType::class, [
                'choices'  =>
                    $types
                ])
            ->add('Rechercher', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        $data = $form->getData();

        $code = $data['code'];

        $type = $data['type'] ?? '';

        $townshipName = str_replace(' ', '%20', $data['township']);

        $response_decoded = $this->callApi($code, $townshipName);

        $etablissementPublicApi = new EtablissementPublicApi($code, $type);

        $getEstablishments = $etablissementPublicApi->getEstablishmentPerTownship();

        if (!is_null(json_decode($getEstablishments)))
        {
            $establishments = json_decode($getEstablishments)->features;

            if($establishments)
                $establishments_hours = $establishments[0]->properties->horaires ?? '';
        }

        if (empty($response_decoded) && $form->isSubmitted())
        {
            $this->addFlash('error', 'La valeur saisis pour un des deux champs n\'est pas correct');
        }

        return $this->render('home.html.twig', [
            'form' => $form->createView(),
            'response' => $response_decoded,
            'etablissements' => $establishments ?? [],
            'establishments_hours' => $establishments_hours ?? []
        ]);
    }

    /**
     * @param $code
     * @param $townshipName
     * @return mixed|\Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function callApi($code, $townshipName)
    {
        $curl = curl_init();

        $options = [CURLOPT_URL => 'https://geo.api.gouv.fr/communes?code=' . $code . '&nom=' . $townshipName, CURLOPT_RETURNTRANSFER => true, ];

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);

        if(curl_getinfo($curl)['http_code'] !== 200) {
            return $this->addFlash('error', 'Il semble qu\'il y es un problème avec l\'api externe.');
        };

        return json_decode($response);
    }
}

