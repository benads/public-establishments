<?php

namespace App\Service;

use Symfony\Component\Routing\Annotation\Route;


class EtablissementPublicApi
{
    public $code;

    public $type;

    public function __construct($code, $type)
    {
        $this->code = $code;
        $this->type = $type;
    }

    public function getEstablishmentPerTownship()
    {
        $curl = curl_init();

        $options = [
            CURLOPT_URL => 'https://etablissements-publics.api.gouv.fr/v3/communes/' . $this->code . '/' . $this->type,
            CURLOPT_RETURNTRANSFER => true,
        ];

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);

        $response_bis_decoded = json_decode(json_encode($response));

        return $response_bis_decoded;

    }
}

